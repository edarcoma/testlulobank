package com.lulobank.challenge.transaction.authorizerapp;

import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lulobank.challenge.transaction.authorizerapp.model.EntryAccount;
import com.lulobank.challenge.transaction.authorizerapp.services.OperationService;
import com.lulobank.challenge.transaction.authorizerapp.utils.SkipSerialisation;

@Profile("!test")
@SpringBootApplication
public class Application implements CommandLineRunner {

	private static Logger log = LoggerFactory.getLogger(Application.class);

	public static final Gson PRETTY_PRINT_JSON = new GsonBuilder().disableHtmlEscaping()
			.addSerializationExclusionStrategy(new ExclusionStrategy() {

				public boolean shouldSkipField(FieldAttributes f) {
					return f.getAnnotation(SkipSerialisation.class) != null;
				}

				public boolean shouldSkipClass(Class<?> clazz) {
					return false;
				}
			}).create();
	
	@Autowired
	private OperationService operationService;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	public void run(String... args) throws Exception {
		log.info("Enter Account or Transaction:\n");
		Scanner scanner = new Scanner(System.in);
		while (scanner.hasNext()) {
			String line = scanner.nextLine();
			if (StringUtils.isNotBlank(line)) {
				EntryAccount entryAccount;

				entryAccount = operationService.processOperation(line);
				if (entryAccount.getAccount() != null) {
					String jsonString = PRETTY_PRINT_JSON.toJson(entryAccount);
					System.out.println("->" + jsonString);					
				}
			}
		}
		scanner.close();

	}
}
