package com.lulobank.challenge.transaction.authorizerapp.model;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Transaction {

	private long id;

	private String merchant;

	private long amount;

	private Date time;
}
