package com.lulobank.challenge.transaction.authorizerapp.services.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.lulobank.challenge.transaction.authorizerapp.model.EntryAccount;
import com.lulobank.challenge.transaction.authorizerapp.model.EntryTransaction;
import com.lulobank.challenge.transaction.authorizerapp.services.AccountService;
import com.lulobank.challenge.transaction.authorizerapp.services.OperationService;
import com.lulobank.challenge.transaction.authorizerapp.services.TransactionService;

@Service
public class OperationImpl implements OperationService {

	private static Logger log = LoggerFactory.getLogger(OperationImpl.class);
	
	@Autowired
	private AccountService accountImpl;
	
	@Autowired
	private TransactionService transactionImpl;
	
	@Override
	public EntryAccount processOperation(String text) {
		EntryAccount entryAccount = new EntryAccount();
		try {
		Optional<Object> validate = validateEntry(text);
		if (validate.isPresent()) {
			Object object = validate.get();
			if (object instanceof EntryAccount) {
				entryAccount = accountImpl.processInformation((EntryAccount) object);
			}else {
				entryAccount = transactionImpl.processInformation((EntryTransaction) object);
			}

		}
		}catch (Exception e) {
			log.info("Invalid operation");
		}
		return entryAccount;
	}

	private Optional<Object> validateEntry(String text) {
		Optional<Object> optional = Optional.empty();
		try {
			Gson json = new Gson();
			EntryAccount entryAccount = json.fromJson(text, EntryAccount.class);
			if (entryAccount.getAccount() != null) {
				optional = Optional.of((Object) entryAccount);
			} else {
				EntryTransaction entryTransaction = json.fromJson(text, EntryTransaction.class);
				if (entryTransaction.getTransaction() != null) {
					optional = Optional.of((Object) entryTransaction);
				}
			}
		} catch (Exception e) {			
			log.info("Invalid operation");
		}

		return optional;
	}
}
