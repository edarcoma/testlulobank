package com.lulobank.challenge.transaction.authorizerapp.services;

import com.lulobank.challenge.transaction.authorizerapp.model.EntryAccount;

public interface AccountService {
	EntryAccount processInformation(EntryAccount account);
}
