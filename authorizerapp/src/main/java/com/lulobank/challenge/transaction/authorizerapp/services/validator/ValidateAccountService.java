package com.lulobank.challenge.transaction.authorizerapp.services.validator;

import java.util.Optional;

import org.javatuples.Pair;

import com.lulobank.challenge.transaction.authorizerapp.model.Account;

public interface ValidateAccountService {
	Pair<Boolean, Optional<Account>> validate(Account account);
}
