package com.lulobank.challenge.transaction.authorizerapp.services.validator.impl;

import java.util.Calendar;
import java.util.Optional;
import java.util.stream.Collectors;

import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lulobank.challenge.transaction.authorizerapp.PropertiesConfig;
import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.model.Transaction;
import com.lulobank.challenge.transaction.authorizerapp.repository.HistoryOperation;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateTransactionService;

@Service("transactionLimit")
public class ValidateTransactionLimitImpl implements ValidateTransactionService {

	@Autowired
	HistoryOperation historyOperation;
	
	@Autowired
	private PropertiesConfig properties;
	
	@Override
	public Pair<Boolean, Optional<Account>> validate(Transaction transaction) {
		Calendar calendarTransaction = Calendar.getInstance();
		Calendar calendarUp = Calendar.getInstance();
		Calendar calendarDown = Calendar.getInstance();
		calendarTransaction.setTime(transaction.getTime());
		int validateTransaction = 0;

		for (Transaction currentTransaction : historyOperation.getTransactionList().stream()
				.filter(s -> s.getId() == transaction.getId()).collect(Collectors.toList())) {
			calendarDown.setTime(currentTransaction.getTime());
			calendarUp.setTime(currentTransaction.getTime());
			calendarUp.add(Calendar.MINUTE, properties.getMaxTimeTransaction());
			calendarDown.add(Calendar.MINUTE, -properties.getMaxTimeTransaction());
			if (calendarTransaction.getTimeInMillis() >= calendarDown.getTimeInMillis()
					&& calendarTransaction.getTimeInMillis() <= calendarUp.getTimeInMillis()) {
				validateTransaction++;
			}
		}

		Optional<Account> accountOp = historyOperation.getAccountList().stream()
				.filter(s -> s.getId() == transaction.getId()).findFirst();

		return new Pair<>(validateTransaction < properties.getMaxTransaction(), accountOp);
	}

}
