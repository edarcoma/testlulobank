package com.lulobank.challenge.transaction.authorizerapp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ConfigurationProperties(prefix = "com.lulobak.challenge.transaction.authorizerapp")
@Configuration
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PropertiesConfig {
	private Integer maxTransaction;
	private Integer maxTimeTransaction;
	private Integer maxRepeatTransaction;
}
