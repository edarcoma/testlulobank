package com.lulobank.challenge.transaction.authorizerapp.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.lulobank.challenge.transaction.authorizerapp.utils.SkipSerialisation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Account implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SkipSerialisation
	@SerializedName(value =  "id")
    private long id;
	
	@SerializedName(value = "active-card")
    private boolean activeCard;
			
	@SerializedName(value = "available-limit")
    private long availableLimit;	
				
	@SerializedName(value = "violations")
	 private List<String> violations;		
}
