package com.lulobank.challenge.transaction.authorizerapp.services.impl;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.SerializationUtils;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.lulobank.challenge.transaction.authorizerapp.constants.ConstantMessage;
import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.model.EntryAccount;
import com.lulobank.challenge.transaction.authorizerapp.model.EntryTransaction;
import com.lulobank.challenge.transaction.authorizerapp.repository.HistoryOperation;
import com.lulobank.challenge.transaction.authorizerapp.services.TransactionService;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateAccountService;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateTransactionService;

@Service
public class TransactionImpl implements TransactionService {

	@Autowired
	private HistoryOperation historyOperation;

	@Autowired
	@Qualifier("accountExist")
	private ValidateAccountService validateExistService;

	@Autowired
	@Qualifier("accountActive")
	private ValidateAccountService validateActiveService;

	@Autowired
	@Qualifier("accountAmount")
	private ValidateAccountService validateAmountService;

	@Autowired
	@Qualifier("transactionLimit")
	private ValidateTransactionService validateLimitService;

	@Autowired
	@Qualifier("transactionRepeat")
	private ValidateTransactionService validateRepeatService;

	@Autowired
	@Qualifier("negativeValues")
	private ValidateTransactionService validateNegativeValues;

	@Override
	public EntryAccount processInformation(EntryTransaction transaction) {
		List<String> errors = new ArrayList<>();
		EntryAccount entryAccount = new EntryAccount();
		Account accountInformation = new Account();
		accountInformation.setId(transaction.getTransaction().getId());
		accountInformation.setAvailableLimit(transaction.getTransaction().getAmount());
		Pair<Boolean, Optional<Account>> resultValidation = validateNegativeValues
				.validate(transaction.getTransaction());
		if (!resultValidation.getValue0().booleanValue() && !resultValidation.getValue1().isPresent()) {
			throw new InvalidParameterException();
		}
		resultValidation = validateExistService.validate(accountInformation);
		if (!resultValidation.getValue0().booleanValue()) {
			errors.add(ConstantMessage.ACCOUNT_NOT_INITIALIZED);
			accountInformation.setViolations(errors);
			entryAccount.setAccount(accountInformation);
			return entryAccount;
		}
		resultValidation = validateActiveService.validate(accountInformation);
		if (!resultValidation.getValue0().booleanValue()) {
			errors.add(ConstantMessage.CARD_NOT_ACTIVE);
			accountInformation = SerializationUtils.clone(resultValidation.getValue1().get());
			accountInformation.setViolations(errors);
			entryAccount.setAccount(accountInformation);
			return entryAccount;
		}
		resultValidation = validateAmountService.validate(accountInformation);
		if (!resultValidation.getValue0().booleanValue()) {
			errors.add(ConstantMessage.INSUFFICIENT_LIMIT);
			accountInformation = SerializationUtils.clone(resultValidation.getValue1().get());
			accountInformation.setViolations(errors);
			entryAccount.setAccount(accountInformation);
			return entryAccount;
		}
		resultValidation = validateLimitService.validate(transaction.getTransaction());
		if (!resultValidation.getValue0().booleanValue()) {
			errors.add(ConstantMessage.HIGH_FREQUENCY_SMALL_INTERVAL);
			accountInformation = SerializationUtils.clone(resultValidation.getValue1().get());
			accountInformation.setViolations(errors);
			entryAccount.setAccount(accountInformation);
			return entryAccount;
		}
		resultValidation = validateRepeatService.validate(transaction.getTransaction());
		if (!resultValidation.getValue0().booleanValue() && resultValidation.getValue1().isPresent()) {
			errors.add(ConstantMessage.DOUBLED_TRANSACTION);
			accountInformation = SerializationUtils.clone(resultValidation.getValue1().get());
			accountInformation.setViolations(errors);
			entryAccount.setAccount(accountInformation);
			return entryAccount;
		}

		Account originalAccount = historyOperation.getAccountList().stream()
				.filter(s -> s.getId() == transaction.getTransaction().getId()).findFirst().get();
		originalAccount
				.setAvailableLimit(originalAccount.getAvailableLimit() - transaction.getTransaction().getAmount());
		entryAccount.setAccount(originalAccount);
		historyOperation.getTransactionList().add(transaction.getTransaction());

		return entryAccount;
	}

}
