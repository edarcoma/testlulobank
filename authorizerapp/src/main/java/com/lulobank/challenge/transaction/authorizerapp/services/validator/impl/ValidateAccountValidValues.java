package com.lulobank.challenge.transaction.authorizerapp.services.validator.impl;

import java.util.Optional;

import org.javatuples.Pair;
import org.springframework.stereotype.Service;

import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateAccountService;

@Service("validValues")
public class ValidateAccountValidValues implements ValidateAccountService {

	@Override
	public Pair<Boolean, Optional<Account>> validate(Account account) {
		return new Pair<>(account.getId() > 0, Optional.empty());
	}

}
