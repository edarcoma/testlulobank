package com.lulobank.challenge.transaction.authorizerapp.services;

import com.lulobank.challenge.transaction.authorizerapp.model.EntryAccount;

public interface OperationService {
	EntryAccount processOperation(String text);
}
