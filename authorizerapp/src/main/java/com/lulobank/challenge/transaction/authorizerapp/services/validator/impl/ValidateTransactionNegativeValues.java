package com.lulobank.challenge.transaction.authorizerapp.services.validator.impl;

import java.util.Optional;

import org.javatuples.Pair;
import org.springframework.stereotype.Service;

import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.model.Transaction;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateTransactionService;

@Service("negativeValues")
public class ValidateTransactionNegativeValues implements ValidateTransactionService {

	@Override
	public Pair<Boolean, Optional<Account>> validate(Transaction transaction) {
		return new Pair<>(transaction.getAmount() >= 0, Optional.empty());
	}

}
