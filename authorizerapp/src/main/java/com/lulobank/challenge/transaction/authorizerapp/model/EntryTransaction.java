package com.lulobank.challenge.transaction.authorizerapp.model;

import lombok.Data;

@Data
public class EntryTransaction {

	private Transaction transaction;
}
