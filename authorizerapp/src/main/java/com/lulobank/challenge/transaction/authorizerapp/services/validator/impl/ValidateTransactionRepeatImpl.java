package com.lulobank.challenge.transaction.authorizerapp.services.validator.impl;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lulobank.challenge.transaction.authorizerapp.PropertiesConfig;
import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.model.Transaction;
import com.lulobank.challenge.transaction.authorizerapp.repository.HistoryOperation;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateTransactionService;

@Service("transactionRepeat")
public class ValidateTransactionRepeatImpl implements ValidateTransactionService {

	@Autowired
	private HistoryOperation historyOperation;
	
	@Autowired
	private PropertiesConfig properties;	

	@Override
	public Pair<Boolean, Optional<Account>> validate(Transaction transaction) {
		int validateRepeat = 0;
		List<Transaction> transactionList = historyOperation.getTransactionList().stream()
				.filter(s -> s.getId() == transaction.getId()
						&& StringUtils.equalsIgnoreCase(s.getMerchant(), transaction.getMerchant())
						&& s.getAmount() == transaction.getAmount())
				.collect(Collectors.toList());
		if (!transactionList.isEmpty()) {
			Calendar calendarTransaction = Calendar.getInstance();
			Calendar calendarUp = Calendar.getInstance();
			Calendar calendarDown = Calendar.getInstance();
			calendarTransaction.setTime(transaction.getTime());
			for (Transaction currentTransaction : transactionList) {

				calendarDown.setTime(currentTransaction.getTime());
				calendarUp.setTime(currentTransaction.getTime());
				calendarUp.add(Calendar.MINUTE, properties.getMaxTimeTransaction());
				calendarDown.add(Calendar.MINUTE, - properties.getMaxTimeTransaction());
				if (calendarTransaction.getTimeInMillis() >= calendarDown.getTimeInMillis()
						&& calendarTransaction.getTimeInMillis() <= calendarUp.getTimeInMillis()) {
					validateRepeat++;
				}
			}
		}
		Optional<Account> accountOp = historyOperation.getAccountList().stream()
				.filter(s -> s.getId() == transaction.getId()).findFirst();
		return new Pair<>(validateRepeat < properties.getMaxRepeatTransaction(), accountOp);
	}

}
