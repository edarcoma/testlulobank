package com.lulobank.challenge.transaction.authorizerapp.services;

import com.lulobank.challenge.transaction.authorizerapp.model.EntryAccount;
import com.lulobank.challenge.transaction.authorizerapp.model.EntryTransaction;

public interface TransactionService {
	EntryAccount processInformation(EntryTransaction transaction);
}
