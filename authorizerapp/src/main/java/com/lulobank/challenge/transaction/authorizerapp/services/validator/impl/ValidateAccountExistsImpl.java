package com.lulobank.challenge.transaction.authorizerapp.services.validator.impl;

import java.util.Optional;

import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.repository.HistoryOperation;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateAccountService;

@Service("accountExist")
public class ValidateAccountExistsImpl implements ValidateAccountService {

	@Autowired
	private HistoryOperation historyOperation;
	
	@Override
	public Pair<Boolean, Optional<Account>> validate(Account account) {
		Optional<Account> accountOp = historyOperation.getAccountList().stream()
				.filter(x -> x.getId() == account.getId()).findFirst();
		return new Pair<>(accountOp.isPresent(), accountOp);
	}

}
