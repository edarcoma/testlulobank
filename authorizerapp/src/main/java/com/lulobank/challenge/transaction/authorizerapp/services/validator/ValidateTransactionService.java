package com.lulobank.challenge.transaction.authorizerapp.services.validator;

import java.util.Optional;

import org.javatuples.Pair;

import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.model.Transaction;

public interface ValidateTransactionService {
	Pair<Boolean, Optional<Account>> validate(Transaction transaction);
}
