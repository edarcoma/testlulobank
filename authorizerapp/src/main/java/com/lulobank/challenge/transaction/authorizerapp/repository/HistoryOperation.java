package com.lulobank.challenge.transaction.authorizerapp.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.model.Transaction;

import lombok.Data;

@Data
@Component
@Scope("singleton")
public class HistoryOperation {
	private List<Account> accountList = new ArrayList<>();

	private List<Transaction> transactionList = new ArrayList<>();
}
