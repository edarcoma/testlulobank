package com.lulobank.challenge.transaction.authorizerapp.services.validator.impl;

import java.util.Optional;

import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.repository.HistoryOperation;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateAccountService;

@Service("accountAmount")
public class ValidateAccountAmountImpl implements ValidateAccountService{

	@Autowired
	HistoryOperation historyOperation;
	
	@Override
	public Pair<Boolean, Optional<Account>> validate(Account account) {
		Optional<Account> accountOp = historyOperation.getAccountList().stream()
				.filter(s -> s.getId() == account.getId() )
				.findFirst();		
		Boolean amount = accountOp.isPresent() && accountOp.get().getAvailableLimit() >= account.getAvailableLimit() ;
		return new Pair<>(amount, accountOp);
	}

}
