package com.lulobank.challenge.transaction.authorizerapp.services.impl;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.SerializationUtils;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.lulobank.challenge.transaction.authorizerapp.constants.ConstantMessage;
import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.model.EntryAccount;
import com.lulobank.challenge.transaction.authorizerapp.repository.HistoryOperation;
import com.lulobank.challenge.transaction.authorizerapp.services.AccountService;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateAccountService;

@Service
public class AccountImpl implements AccountService {

	@Autowired
	private HistoryOperation historyOperation;

	@Autowired
	@Qualifier("validValues")
	ValidateAccountService validateNegativeValues;

	@Autowired
	@Qualifier("accountExist")
	private ValidateAccountService validateAccountExist;

	@Override
	public EntryAccount processInformation(EntryAccount account) {
		EntryAccount newAccount = new EntryAccount();
		List<String> errors = new ArrayList<>();
		Pair<Boolean, Optional<Account>> resultValidate = validateNegativeValues.validate(account.getAccount());
		if (!resultValidate.getValue0().booleanValue() && !resultValidate.getValue1().isPresent()) {
			throw new InvalidParameterException();
		}
		resultValidate = validateAccountExist.validate(account.getAccount());
		if (resultValidate.getValue0().booleanValue() && resultValidate.getValue1().isPresent()) {
			errors.add(ConstantMessage.ACCOUNT_ALREADY_INITIALIZED);
			newAccount.setAccount(SerializationUtils.clone(resultValidate.getValue1().get()));
		} else {
			historyOperation.getAccountList().add(account.getAccount());
			newAccount.setAccount(SerializationUtils.clone(account.getAccount()));
		}
		newAccount.getAccount().setViolations(errors);
		return newAccount;
	}

}
