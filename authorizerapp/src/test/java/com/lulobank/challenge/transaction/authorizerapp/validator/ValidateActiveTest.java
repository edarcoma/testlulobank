package com.lulobank.challenge.transaction.authorizerapp.validator;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.lulobank.challenge.transaction.authorizerapp.ConfigurationForTest;
import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.repository.HistoryOperation;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateAccountService;

@ActiveProfiles("test")
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
@ContextConfiguration(classes = ConfigurationForTest.class, loader = AnnotationConfigContextLoader.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ValidateActiveTest {

	private static Logger log = LoggerFactory.getLogger(ValidateExistTest.class);
	
	@Autowired
	@Qualifier("accountActive")
	private ValidateAccountService validate;
	
	@Autowired
	private HistoryOperation historyOperation;
	
	@Test
	@Order(30)
	void testNotActiveAccount() {
		historyOperation.getAccountList().add(Account.builder().id(30).activeCard(false).availableLimit(150).build());
		var result = validate.validate(Account.builder().id(30).activeCard(true).availableLimit(150).build());
		log.info("testNotActiveAccount:{}", result.getValue0().booleanValue());
		assertEquals(false, result.getValue0().booleanValue());
	}
	
	@Test
	@Order(31)
	void testActiveAccount() {
		historyOperation.getAccountList().add(Account.builder().id(31).activeCard(true).availableLimit(150).build());
		var result = validate.validate(Account.builder().id(31).build());
		log.info("testActiveAccount:{}", result.getValue0().booleanValue());
		assertEquals(true, result.getValue0().booleanValue());
	}

}
