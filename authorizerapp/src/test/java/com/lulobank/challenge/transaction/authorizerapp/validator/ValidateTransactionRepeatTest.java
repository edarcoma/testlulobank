package com.lulobank.challenge.transaction.authorizerapp.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Calendar;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.lulobank.challenge.transaction.authorizerapp.ConfigurationForTest;
import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.model.Transaction;
import com.lulobank.challenge.transaction.authorizerapp.repository.HistoryOperation;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateTransactionService;

@ActiveProfiles("test")
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
@ContextConfiguration(classes = ConfigurationForTest.class, loader = AnnotationConfigContextLoader.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ValidateTransactionRepeatTest {

	private static Logger log = LoggerFactory.getLogger(ValidateExistTest.class);

	@Autowired
	@Qualifier("transactionRepeat")
	private ValidateTransactionService validate;

	@Autowired
	private HistoryOperation historyOperation;
	
	@Test
	@Order(70)
	void testSameAmmountDifferentMerchant() {
		Calendar cal = Calendar.getInstance();
		historyOperation.getAccountList().add(Account.builder().id(70).activeCard(true).availableLimit(500).build());
		historyOperation.getTransactionList()
		.add(Transaction.builder().id(70).amount(100).merchant("Burger King").time(cal.getTime()).build());
		var result = validate.validate(Transaction.builder().id(70).amount(10).merchant("Burger King").time(cal.getTime()).build());
		log.info("testSameAmmountDifferentMerchant: {}", result.getValue0().booleanValue());
		assertEquals(true, result.getValue0().booleanValue());
	}
	
	@Test
	@Order(70)
	void testRepeatTransaction() {
		Calendar cal = Calendar.getInstance();
		historyOperation.getAccountList().add(Account.builder().id(70).activeCard(true).availableLimit(500).build());
		historyOperation.getTransactionList()
		.add(Transaction.builder().id(70).amount(100).merchant("Burger King").time(cal.getTime()).build());
		var result = validate.validate(Transaction.builder().id(70).amount(100).merchant("Burger King").time(cal.getTime()).build());
		log.info("testRepeatTransaction: {}", result.getValue0().booleanValue());
		assertEquals(false, result.getValue0().booleanValue());
	}

}
