package com.lulobank.challenge.transaction.authorizerapp.validator;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Calendar;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.lulobank.challenge.transaction.authorizerapp.ConfigurationForTest;
import com.lulobank.challenge.transaction.authorizerapp.model.Transaction;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateTransactionService;

@ActiveProfiles("test")
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
@ContextConfiguration(classes = ConfigurationForTest.class, loader = AnnotationConfigContextLoader.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ValidateTransactionNegativeValuesTest {

	private static Logger log = LoggerFactory.getLogger(ValidateExistTest.class);

	@Autowired
	@Qualifier("negativeValues")
	private ValidateTransactionService validate;

	@Test
	@Order(60)
	void testPositiveValue() {
		var result = validate.validate(Transaction.builder().id(60).amount(30).merchant("Corral")
				.time(Calendar.getInstance().getTime()).build());
		log.info("testPositiveValue: {}", result.getValue0().booleanValue());
		assertEquals(true, result.getValue0().booleanValue());
	}

	@Test
	@Order(61)
	void testNegativeValue() {
		var result = validate.validate(Transaction.builder().id(61).amount(-30).merchant("Corral")
				.time(Calendar.getInstance().getTime()).build());
		log.info("testNegativeValue: {}", result.getValue0().booleanValue());
		assertEquals(false, result.getValue0().booleanValue());
	}
}
