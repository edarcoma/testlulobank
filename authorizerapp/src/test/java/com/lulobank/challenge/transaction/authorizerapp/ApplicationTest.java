package com.lulobank.challenge.transaction.authorizerapp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.lulobank.challenge.transaction.authorizerapp.constants.ConstantMessage;
import com.lulobank.challenge.transaction.authorizerapp.model.EntryAccount;
import com.lulobank.challenge.transaction.authorizerapp.services.OperationService;

@ActiveProfiles("test")
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
@ContextConfiguration(classes = ConfigurationForTest.class, loader = AnnotationConfigContextLoader.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ApplicationTest {

	private static Logger log = LoggerFactory.getLogger(ApplicationTest.class);

	@Qualifier("operationService")
	@Autowired(required = true)
	private OperationService operationService;

	@Test
	@Order(0)
	void testBadRequest() {
		EntryAccount entry = operationService.processOperation("XXXYXYYYYYAYAY");
		log.info("testBadRequest:{}", entry);
		assertNull(entry.getAccount());
	}

	@Test
	@Order(1)
	void testAccountResgister() {

		EntryAccount entry = operationService
				.processOperation("{\"account\": {\"id\": 1, \"active-card\": true, \"available-limit\": 100}}");
		log.info("testAccountResgister:{}", entry);
		assertEquals(1, entry.getAccount().getId());
	}

	@Test
	@Order(2)
	void testAccountAlreadyExist() {
		EntryAccount entry = operationService
				.processOperation("{\"account\": {\"id\": 2, \"active-card\": true, \"available-limit\": 100}}");
		entry = operationService
				.processOperation("{\"account\": {\"id\": 2, \"active-card\": true, \"available-limit\": 100}}");
		log.info("testAccountAlreadyExist:{}", entry);
		assertEquals(ConstantMessage.ACCOUNT_ALREADY_INITIALIZED, entry.getAccount().getViolations().get(0));
	}

	@Test
	@Order(3)
	void testaccountNotInitialized() {
		EntryAccount entry = operationService.processOperation(
				"{\"transaction\": {\"id\": 3, \"merchant\": \"Burger King\", \"amount\": 20, \"time\":\"2019-02-13T10:00:00.000Z\"}}");
		log.info("testaccountNotInitialized:{}", entry);
		assertEquals(ConstantMessage.ACCOUNT_NOT_INITIALIZED, entry.getAccount().getViolations().get(0));
	}

	@Test
	@Order(4)
	void testCardNotActive() {
		EntryAccount entry = operationService
				.processOperation("{\"account\": {\"id\": 4, \"active-card\": false, \"available-limit\": 100}}");
		entry = operationService.processOperation(
				"{\"transaction\": {\"id\": 4, \"merchant\": \"Burger King\", \"amount\": 20, \"time\":\"2019-02-13T10:00:00.000Z\"}}");
		log.info("testCardNotActive:{}", entry);
		assertEquals(ConstantMessage.CARD_NOT_ACTIVE, entry.getAccount().getViolations().get(0));
	}
	
	@Test
	@Order(4)
	void testInsufficientLimit() {
		EntryAccount entry = operationService
				.processOperation("{\"account\": {\"id\": 5, \"active-card\": true, \"available-limit\": 100}}");
		entry = operationService.processOperation(
				"{\"transaction\": {\"id\": 5, \"merchant\": \"Burger King\", \"amount\": 200, \"time\":\"2019-02-13T10:00:00.000Z\"}}");
		log.info("testInsufficientLimit:{}", entry);
		assertEquals(ConstantMessage.INSUFFICIENT_LIMIT, entry.getAccount().getViolations().get(0));
	}
	
	@Test
	@Order(5)
	void testHighFrequency() {
		EntryAccount entry = operationService
				.processOperation("{\"account\": {\"id\": 6, \"active-card\": true, \"available-limit\": 100}}");
		entry = operationService.processOperation(
				"{\"transaction\": {\"id\": 6, \"merchant\": \"Corral\", \"amount\": 15, \"time\": \"2019-02-13T10:00:00.000Z\"}}");
		entry = operationService.processOperation(
				"{\"transaction\": {\"id\": 6, \"merchant\": \"Burger King\", \"amount\": 20, \"time\":\"2019-02-13T10:00:00.000Z\"}}");
		entry = operationService.processOperation(
				"{\"transaction\": {\"id\": 6, \"merchant\": \"Habbib's\", \"amount\": 10, \"time\": \"2019-02-13T10:01:00.000Z\"}}");
		entry = operationService.processOperation(
				"{\"transaction\": {\"id\": 6, \"merchant\": \"Frisby\", \"amount\": 13, \"time\":\"2019-02-13T10:00:00.000Z\"}}");
		log.info("testHighFrequency:{}", entry);
		assertEquals(ConstantMessage.HIGH_FREQUENCY_SMALL_INTERVAL, entry.getAccount().getViolations().get(0));
	}
	
	@Test
	@Order(6)
	void testDoubledTransaction() {
		EntryAccount entry = operationService
				.processOperation("{\"account\": {\"id\": 7, \"active-card\": true, \"available-limit\": 100}}");
		entry = operationService.processOperation(
				"{\"transaction\": {\"id\": 7, \"merchant\": \"Corral\", \"amount\": 15, \"time\": \"2019-02-13T10:00:00.000Z\"}}");
		entry = operationService.processOperation(
				"{\"transaction\": {\"id\": 7, \"merchant\": \"Corral\", \"amount\": 15, \"time\":\"2019-02-13T10:01:00.000Z\"}}");		
		log.info("testDoubledTransaction:{}", entry);
		assertEquals(ConstantMessage.DOUBLED_TRANSACTION, entry.getAccount().getViolations().get(0));
	}

	@Test
	@Order(7)
	void test() {		
		var result = operationService.processOperation("{\"account\": {\"id\": 0, \"active-card\": true, \"available-limit\": 100}}");
		assertNull(result.getAccount());
	}
}
