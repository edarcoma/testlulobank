package com.lulobank.challenge.transaction.authorizerapp;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.lulobank.challenge.transaction.authorizerapp.repository.HistoryOperation;
import com.lulobank.challenge.transaction.authorizerapp.services.AccountService;
import com.lulobank.challenge.transaction.authorizerapp.services.OperationService;
import com.lulobank.challenge.transaction.authorizerapp.services.TransactionService;
import com.lulobank.challenge.transaction.authorizerapp.services.impl.AccountImpl;
import com.lulobank.challenge.transaction.authorizerapp.services.impl.OperationImpl;
import com.lulobank.challenge.transaction.authorizerapp.services.impl.TransactionImpl;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateAccountService;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateTransactionService;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.impl.ValidateAccountActiveImpl;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.impl.ValidateAccountAmountImpl;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.impl.ValidateAccountExistsImpl;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.impl.ValidateAccountValidValues;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.impl.ValidateTransactionLimitImpl;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.impl.ValidateTransactionNegativeValues;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.impl.ValidateTransactionRepeatImpl;

@Configuration
public class ConfigurationForTest {
	@Bean(name = "operationService")
	public OperationService operationService() {
		return new OperationImpl();
	}

	@Bean(name = "transactionService")
	public TransactionService transactionService() {
		return new TransactionImpl();
	}

	@Bean(name = "accountService")
	public AccountService accountService() {
		return new AccountImpl();
	}

	@Bean(name = "accountExist")
	public ValidateAccountService validateAccountExist() {
		return new ValidateAccountExistsImpl();
	}

	@Bean(name = "historyOperation")
	public HistoryOperation historyOperation() {
		return new HistoryOperation();
	}

	@Bean(name = "transactionLimit")
	public ValidateTransactionService transactionLimit() {
		return new ValidateTransactionLimitImpl();
	}

	@Bean(name = "accountActive")
	public ValidateAccountService accountActive() {
		return new ValidateAccountActiveImpl();
	}

	@Bean(name = "accountAmount")
	public ValidateAccountService accountAmount() {
		return new ValidateAccountAmountImpl();
	}

	@Bean(name = "transactionRepeat")
	public ValidateTransactionService transactionRepeat() {
		return new ValidateTransactionRepeatImpl();
	}
	
	@Bean(name = "validValues")
	public ValidateAccountService validValues() {
		return new ValidateAccountValidValues();
	}
	
	@Bean(name = "negativeValues")
	public ValidateTransactionService transactionnegativeValues() {
		return new ValidateTransactionNegativeValues();
	}

	@Bean
	public PropertiesConfig propertiesConfig() {
		return new PropertiesConfig(3, 2, 1);
	}	

}
