package com.lulobank.challenge.transaction.authorizerapp.validator;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.lulobank.challenge.transaction.authorizerapp.ConfigurationForTest;
import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.model.Transaction;
import com.lulobank.challenge.transaction.authorizerapp.repository.HistoryOperation;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateTransactionService;

@ActiveProfiles("test")
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
@ContextConfiguration(classes = ConfigurationForTest.class, loader = AnnotationConfigContextLoader.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ValidateTransactionLimitTest {

	private static Logger log = LoggerFactory.getLogger(ValidateExistTest.class);

	@Autowired
	@Qualifier("transactionLimit")
	private ValidateTransactionService validate;

	@Autowired
	private HistoryOperation historyOperation;

	@Test
	@Order(50)
	void testValidLimit() {
		Calendar cal = Calendar.getInstance();
		historyOperation.getAccountList().add(Account.builder().id(50).activeCard(true).availableLimit(500).build());
		historyOperation.getTransactionList()
				.add(Transaction.builder().id(50).amount(100).merchant("Burger King").time(cal.getTime()).build());
		historyOperation.getTransactionList()
				.add(Transaction.builder().id(50).amount(10).merchant("Burger King").time(cal.getTime()).build());
		var result = validate
				.validate(Transaction.builder().id(50).amount(10).merchant("Corral").time(cal.getTime()).build());
		log.info("testValidLimit: {}", result.getValue0().booleanValue());
		assertEquals(true, result.getValue0().booleanValue());
	}
	
	@Test
	@Order(51)
	void testNotValidLimit() {
		Calendar cal = Calendar.getInstance();
		historyOperation.getAccountList().add(Account.builder().id(51).activeCard(true).availableLimit(500).build());
		historyOperation.getTransactionList()
				.add(Transaction.builder().id(51).amount(100).merchant("Burger King").time(cal.getTime()).build());
		historyOperation.getTransactionList()
				.add(Transaction.builder().id(51).amount(10).merchant("Burger King").time(cal.getTime()).build());
		historyOperation.getTransactionList()
		.add(Transaction.builder().id(51).amount(10).merchant("MC Donalds").time(cal.getTime()).build());
		var result = validate
				.validate(Transaction.builder().id(51).amount(10).merchant("Corral").time(cal.getTime()).build());
		log.info("testNotValidLimit: {}", result.getValue0().booleanValue());
		assertEquals(false, result.getValue0().booleanValue());
	}

}
