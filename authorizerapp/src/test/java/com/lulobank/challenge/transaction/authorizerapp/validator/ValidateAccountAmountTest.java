package com.lulobank.challenge.transaction.authorizerapp.validator;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.lulobank.challenge.transaction.authorizerapp.ConfigurationForTest;
import com.lulobank.challenge.transaction.authorizerapp.model.Account;
import com.lulobank.challenge.transaction.authorizerapp.repository.HistoryOperation;
import com.lulobank.challenge.transaction.authorizerapp.services.validator.ValidateAccountService;

@ActiveProfiles("test")
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
@ContextConfiguration(classes = ConfigurationForTest.class, loader = AnnotationConfigContextLoader.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ValidateAccountAmountTest {

	private static Logger log = LoggerFactory.getLogger(ValidateExistTest.class);
	
	@Autowired
	@Qualifier("accountAmount")
	private ValidateAccountService validate;
	
	@Autowired
	private HistoryOperation historyOperation;
	
	@Test
	@Order(40)
	void testEnoughtAmount() {
		historyOperation.getAccountList().add(Account.builder().id(40).activeCard(true).availableLimit(150).build());
		var result = validate.validate(Account.builder().id(40).activeCard(true).availableLimit(100).build());
		log.info("testEnoughtAmount: {}", result.getValue1().get().getAvailableLimit());
		assertEquals(true, result.getValue0().booleanValue());
	}

	@Test
	@Order(41)
	void testNotEnoughtAmount() {
		historyOperation.getAccountList().add(Account.builder().id(41).activeCard(true).availableLimit(150).build());
		var result = validate.validate(Account.builder().id(41).activeCard(true).availableLimit(200).build());
		log.info("testNotEnoughtAmount: {}", result.getValue1().get().getAvailableLimit());
		assertEquals(false, result.getValue0().booleanValue());
	}
}
